Recruitment Bluemedia Client
============================

**[!] Package is no longer available on [packagist.org](https://packagist.org) so there is no possibility to install it by Composer.**

[!] Package was renamed from *Blue Services* to *Bluemedia* but code uses *Blue Services* names. It was done because *Bluemedia* is parent company of *Blue Services* and it can be found on the internet.

---

Recruitment Bluemedia Client is a part of recruitment task for Bluemedia company.

It is a frontend application that allows manage items.

# Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Server](#server)
- [License](#license)

## Installation

```sh
composer require gaqateq/recruitment-blue-services-client
```

Application needs REST API with `/items` endpoint to work. You can also use ready package for this, see section [Server](#server).

## Usage

After installation package is ready to use. Application is available via URL `/items`.

## Server

As mentioned in the header this package is one part of the task. There is available a Server for this application which is available [here](https://gitlab.com/venomoth-recruitment/bluemedia.pl-2018-server).

## License

Recruitment Bluemedia Client is released under the MIT Licence. See the bundled LICENSE file for details.
